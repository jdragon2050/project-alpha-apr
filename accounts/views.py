from django.shortcuts import render, redirect
from .forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


# Create your views here.
def login_view(request):
    form = LoginForm()
    if request.method == "GET":

        return render(request, "accounts/login.html", {"form": form})
    username = request.POST.get("username")
    password = request.POST.get("password")
    if username and password:
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("list_projects")
        else:
            return render(request, "accounts/login.html", {"form": form})


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    form = SignupForm()
    if request.method == "GET":
        return render(request, "accounts/signup.html", {"form": form})
    else:
        username = request.POST.get("username")
        password = request.POST.get("password")
        password_confirmation = request.POST.get("password_confirmation")
        if username and password and password_confirmation:
            if password != password_confirmation:
                return render(
                    request,
                    "accounts/signup.html",
                    {"form": form, "error": "The passwords do not match"},
                )
            else:
                try:
                    user = User.objects.create_user(
                        username, password=password
                    )
                    login(request, user)
                    return redirect("list_projects")
                except Exception as e:
                    return render(
                        request,
                        "accounts/signup.html",
                        {"form": form, "error": str(e)},
                    )
        else:
            return render(
                request,
                "accounts/signup.html",
                {"form": form, "error": "Please fill out all fields"},
            )
