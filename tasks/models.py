from django.db import models


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField("date started")
    due_date = models.DateTimeField("date due")
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project", on_delete=models.CASCADE, related_name="tasks"
    )
    assignee = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, related_name="tasks"
    )
