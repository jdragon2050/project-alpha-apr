from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


# Create your views here.
@login_required
def show_projects(request):
    projects = Project.objects.filter(owner=request.user)
    return render(request, "projects/projects.html", {"projects": projects})


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    return render(request, "projects/project.html", {"project": project})


from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from .models import Project


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("/projects/")
    else:
        form = ProjectForm()
    return render(request, "projects/create_project.html", {"form": form})
